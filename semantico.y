/*ESCUELA POLITECNICA NACIONAL
           FACULTAD DE SISTEMAS

            Analizardo Sintactico
Por:
Jairo Vera
Kevin Jimenez   
*/

%{
       #include <stdio.h>
       #include <stdlib.h>	
	#include <string.h>
	#include<stdbool.h>      
void yyerror(char *);

extern FILE *yyin;
extern FILE *yyout;
extern FILE *datos;
extern FILE *base;
extern int num_lines;
extern int yyparse(void);
extern int yylex(void);
extern char* yytext;
int errors;
char arreglo[100000] ;
int num_par=0;
int num_parIngreso=0;

void setVector(int ingreso){
for(int i=0;i<ingreso;i++){
fprintf(datos,",0");
}
fprintf(datos,"\n");

}

void setEstructura(char* QueEs){
fprintf(datos,",%s\n",QueEs);
}


bool seRepite(char* tipo, char* identificador)
{
char linea [1000];
char* token;
datos=fopen("datos.csv","r+");

while(fgets(linea,1000,datos)){
token= strtok(linea,",");
if(strcmp(token,identificador)==0)
{
return true;
}
}
return false;
}

int cuantosPar(char* Funcion){
int salida=0;
char linea[1000];

char* token1;
char* token2;
datos=fopen("datos.csv","r+");

while(fgets(linea,1000,datos)){
token1=strtok(linea,",");
if(strcmp(token1,Funcion)==0)
{
for(int i=0;i<3;i++){
token1=strtok(NULL,",");
}
salida=atoi(token1);
}
}

return salida;
}



void insertar(char* tipo, char* identificador,int length,char* estructura){
datos=fopen("datos.csv","a+");
fprintf(datos,"%s,%s,%s,%d\n",identificador,tipo,estructura,length);
fclose(datos);
}


void insertarFuncion(char* tipo, char* identificador, char* parametros){
datos=fopen("datos.csv","a+");
if (strcmp(parametros,"")!=0){
fprintf(datos,"%s,%s,Funcion,%d,%s\n",identificador,tipo,num_par,parametros);
}
else
{fprintf(datos,"%s,%s,Funcion,%d\n",identificador,tipo,num_par);}

arreglo[0]=0;
num_par=0;
fclose(datos);

}




char* parametroString(char* parametro, char* tipo, char* identificador){
char* coma=","; 


if(strcmp(parametro,"")!=0)
{
strcpy(arreglo,parametro);
strcat(arreglo,coma);
}
strcat(arreglo,identificador);
strcat(arreglo,coma);
strcat(arreglo,tipo);
return arreglo;
}



void errorRepetido(char* identificador){
errors++;
printf("\n------Error de tipo: 'semantico', el identificador: '%s', en la linea: %d, ya existe.------ \n\n",identificador,num_lines);
arreglo[0]=0;
num_par=0;
}

void errorLLamada(char* funcion){
errors++;
printf("\n------Error de tipo 'semantico', la funcion: '%s', en la linea: %d, no existe.------\n\n ",funcion,num_lines);
arreglo[0]=0;
num_par=0;
}

void errorParametro(char * funcion){
errors++;
printf("\n------Error de tipo 'semantico', la llamada a la funcion: '%s', en la linea: %d, no tiene los argumentos necesarios.------\n\n ",funcion,num_lines);
arreglo[0]=0;
num_par=0;
num_parIngreso;
}


%}

%left INPUT
%left OUTPUT
%left IF
%left THEN
%left ELSE
%left RETURN
%left DO
%left WHILE
%token OPERACIONSUMA
%token OPERACIONRESTA
%token OPERACIONMULT
%token OPERACIONDIVD 
%left OPERACIONSUMA OPERACIONRESTA
%left OPERACIONMULT OPERACIONDIVD
%left <tipo>TIPO_DATO
%token <booleano>BOOLEANO
%token CARACTER_ESPECIAL

%left KEYOP
%right KEYCL

%left ParetOP
%right ParetCL

%left BracketOP
%right BracketCL

%left COMPARADOR
%left OP_LOGICA
%token <identificador>IDENTIFICADOR
%token <integral>INTEGER
%token <flotante>FLOAT
%token <letra> CHAR
%token <string>STRING

%token ASIGNACION
%left  COMA
%right FinCommand

%left IGUAL



%union {
int integral;
float flotante;
char* booleano;
char* string;
char* tipo;
char* identificador;
char* letra;
char* parametro;
}


%type<parametro>parametro 
%type<flotante>operacionFloat
%type<integral>operacionInt
%type <integral>parIngreso
%%

expr:
	decGlobal FinCommand
	| metodo FinCommand
	| expr decGlobal FinCommand
	| expr metodo FinCommand
	| operacion FinCommand
	| expr operacion FinCommand
	| llamarFuncion FinCommand
	| expr llamarFuncion FinCommand
	| cambio FinCommand
	| expr cambio FinCommand
	| error
	;
 cambio:
	IDENTIFICADOR ASIGNACION IGUAL INTEGER   {}   
	;



operacion:
	operacionFloat
	|operacionInt
	;	

ListaOPeracion:
	OPERACIONSUMA
	| OPERACIONRESTA
	| OPERACIONMULT
	| OPERACIONDIVD
	;

operacionInt:
	IDENTIFICADOR ListaOPeracion IDENTIFICADOR   {if (seRepite($1,$3)&&seRepite($3,$1))
printf("si se puede operar entre %s y %s",$1,$3);}
	| INTEGER						{$$=$1;}	
	| operacionInt OPERACIONSUMA operacionInt		{$$=$1+$3;}	
	| operacionInt OPERACIONRESTA operacionInt		{$$=$1-$3;}	 
	| operacionInt OPERACIONMULT operacionInt		{$$=$1*$3;}		
	| ParetOP operacionInt ParetCL			        {$$=$2;}	
	;


operacionFloat:
	IDENTIFICADOR ListaOPeracion IDENTIFICADOR   {if (seRepite($1,$3)&&seRepite($3,$1))
printf("si se puede operar entre %s y %s",$1,$3);}
	|FLOAT							{$$=$1;}	
	| operacionFloat OPERACIONSUMA operacionFloat		{$$=$1+$3;}	
	| operacionFloat OPERACIONRESTA operacionFloat		{$$=$1-$3;}	 
	| operacionFloat OPERACIONMULT operacionFloat		{$$=$1*$3;}	
	| operacionFloat OPERACIONDIVD operacionFloat 		{$$=$1/$3;}	
	| ParetOP operacionFloat ParetCL			{$$=$2;}	
	;
	






decGlobal:
	decGlobalSencilla 
	| TIPO_DATO ASIGNACION IDENTIFICADOR BracketOP INTEGER BracketCL    {if(!seRepite($1,$3)){insertar($1,$3,$5,"Vector");}
else{errorRepetido($3);}} 
	;

decGlobalSencilla:
	TIPO_DATO ASIGNACION IDENTIFICADOR   {if(!seRepite($1,$3)){insertar($1,$3,1,"Unico");}else{errorRepetido($3);}} 
	;

metodo:
	TIPO_DATO ASIGNACION IDENTIFICADOR ParetOP ParetCL   {if(!seRepite($1,$3)){insertarFuncion($1,$3,"");}
								else{errorRepetido($3);}}
	|TIPO_DATO ASIGNACION IDENTIFICADOR ParetOP parametro ParetCL    {if(!seRepite($1,$3)){
									
									insertarFuncion($1,$3,$5);
									}else
									{errorRepetido($3);}
									}  
	;

parametro:
	TIPO_DATO ASIGNACION IDENTIFICADOR                        {$$=parametroString("",$1,$3);num_par++;}               
	|parametro COMA TIPO_DATO ASIGNACION IDENTIFICADOR     	  {$$=parametroString($$,$3,$5);num_par++;} 
;


llamarFuncion:
	IDENTIFICADOR ParetOP ParetCL                     {if(seRepite($1,$1)&&(cuantosPar($1)==0)){printf("llamada correcta\n");num_parIngreso=0;}
							  else {errorParametro($1);}
							  }
	|IDENTIFICADOR ParetOP parIngreso ParetCL      {if(seRepite($1,$1)&&(cuantosPar($1)==$3)){printf("llamada correcta\n");num_parIngreso=0;}
							 else {errorParametro($1);}}
	;

parIngreso:
	dato                                   {num_parIngreso++;$$=num_parIngreso;}
	| parIngreso COMA dato			{num_parIngreso++;$$=num_parIngreso;}	
	;
dato:
	INTEGER
	| FLOAT
	| BOOLEANO
	| STRING
	| CHAR
	;

%%

void yyerror(char *s){

errors++;
printf("\n------Error de tipo: '%s' en '%s' en la Linea #%d------\n\n",s,yytext,num_lines);
}

int main(){
   
        if ( "ingreso.txt" > 0)
             { yyin = fopen ("ingreso.txt", "r");}
        else
                {yyin = stdin;}
        yyout = fopen("salida.txt","w");
        do{
        yyparse();}
        while(!feof(yyin));
        if(errors==0)
                printf("\nCompilacion Finalizada Sin Errores\n\n");
        else
                printf("\nCompilacion Finalizada con Errores\n\n");

return 0;}


                                                                              


