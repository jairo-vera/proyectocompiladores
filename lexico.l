/*ESCUELA POLITECNICA NACIONAL
           FACULTAD DE SISTEMAS

            Analizardo Lexico
Por:
Jairo Vera
Kevin Jimenez   
*/


%{
#include <stdlib.h>
#include "semantico.tab.h"
FILE *datos;
FILE *base;
int num_lines=1;
int errors=0;
void yyerror(char *);

%}


%%

\n                                      {num_lines++;}
"input"                                 {return INPUT;}
"output"                                {return OUTPUT; }
"if"                                    {return IF;}
"then"                                  {return THEN;}
"else"                                  {return ELSE;}
"return"                                {return RETURN;}
"do"                                    {return DO;}
"while"                                 {return WHILE;}
"+"					{return OPERACIONSUMA;}
"-"					{return OPERACIONRESTA;}
"*"					{return OPERACIONMULT;}
"/"					{return OPERACIONDIVD;}
"int"|"float"|bool|"char"|"string"      {yylval.tipo=strdup(yytext);
                                        return TIPO_DATO;}
"true"|"false"                          {yylval.booleano=strdup(yytext);
                                        return BOOLEANO;}
[*$&]                                   {return CARACTER_ESPECIAL;}
"="                                     {return IGUAL;}
";"                                     {return FinCommand;}
","                                     {return COMA;}
":"                                     {return ASIGNACION;}
"{"                                     {return KEYOP;}
"}"                                     {return KEYCL;}
"("                                     {return ParetOP;}
")"                                     {return ParetCL;}
"["                                     {return BracketOP;}
"]"                                     {return BracketCL;}
">"|"<"|"<="|">="|"!="|"=="             {return COMPARADOR;}
"&&"|"||"|"!"                           {return OP_LOGICA;}
[a-z][a-zA-Z0-9]*                               {yylval.identificador=strdup(yytext);
                                        return IDENTIFICADOR;}
[0-9]+                                  {yylval.integral = atoi(yytext);
                                        return INTEGER;}
[0-9]*\.[0-9]+                  {yylval.flotante=atoi(yytext); return FLOAT;}
('.')                                   {yylval.tipo=strdup(yytext); return CHAR;}
(\".*\")                                {yylval.string=strdup(yytext); return STRING;}

(\/\/.*)                                                {/*comentario de una linea*/;}
(\/\*(.|\n)*\*\/)                                       {/*comentario de multiple linea*/;}
[ \t]                                                   ;
.                                                       {yyerror("lexical error");errors++;}

%%

int yywrap(void) {
    return 1;
}

                                              